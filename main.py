import configparser
import datetime
import logging
import re
import sys
from pathlib import Path

from src.launch_driver import set_driver_config as initiate_scraping
from utilities import UNIQUE_ID


def enable_logging(config_file, logging_level=logging.INFO):
    # CONFIGURING LOG FILE
    log_folder = Path(Path(config_file['PATHS']['log_path']), Path('scraping'))
    log_folder.mkdir(parents=True, exist_ok=True)
    log_file = Path(Path(log_folder), Path("{}.log".format(UNIQUE_ID)))

    # SETTING LOGGING PARAMETERS
    logging.basicConfig(filename=log_file, level=logging.INFO, filemode='w',
                        format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')


if __name__ == '__main__':
    # READING CONFIG FILE
    config = configparser.ConfigParser()
    config.read('config')

    # ENABLE LOGGING
    enable_logging(config)

    logging.info("INPUT PARAMETERS\n" + "*" * 100 + "\n" + "\n".join(sys.argv[1:]) + "\n" + "*" * 100)
    logging.info('STARTING PROCESS')

    task_type = sys.argv[1]
    if task_type == 'scraping':
        business_center = sys.argv[2]
        initiate_scraping(config, business_center)
