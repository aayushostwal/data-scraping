import os
import os.path
from pathlib import Path

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from utilities import UNIQUE_ID


def set_driver_config(config, business_center):
    out_path = Path(Path(config['PATHS']['out_path']), Path('scraping'), Path(UNIQUE_ID), Path('data'))
    out_path.mkdir(parents=True, exist_ok=True)

    exe_path = config['PATHS']['chromedriver']

    caps = DesiredCapabilities.CHROME
    caps['goog:loggingPrefs'] = {'performance': 'ALL'}

    option = webdriver.ChromeOptions()
    option.add_argument('--incognito')
    preferences = {"profile.default_content_settings.popups": 0,
                   "download.default_directory": str(os.path.abspath(out_path)) + "\\",
                   "directory_upgrade": True}
    option.add_experimental_option("prefs", preferences)
    option.add_experimental_option('useAutomationExtension', False)
    option.add_argument('--disable-blink-features=AutomationControlled')
    option.add_experimental_option("excludeSwitches", ['enable-automation'])

    imp = getattr(__import__('src.scraping.' + business_center, fromlist=['launch']), 'launch')
    imp.launch(exe_path, option, caps, os.path.split(out_path)[0])
