import time
from alive_progress import alive_bar
from src.scraping.bseindia.util import *


def launch(exe_path, option, caps, out_folder):
    logging.info('INITIATING SCRAPING FOR BSE INDIA')
    driver = open_chrome_instance(exe_path, option, caps)
    scripts = get_all_scripts_bse_india(driver)
    start_time = time.time()
    total = len(scripts)
    with alive_bar(total) as bar:
        for script in scripts:
            try:
                script_id = script['SCRIP_CD']

                download_script_data_bse_india(driver, script_id)
                logging.info(f"DATA DOWNLOADED- ID: {script['SCRIP_CD']:7} | NAME: {script['Scrip_Name']}")

                get_script_portfolio(script, out_folder)
                time.sleep(2)
                logging.info("{}".format("=" * 60))
            except Exception as e:
                logging.warning('FAILED FOR SCRIPT {} : {}'.format(script['Scrip_Name'], e))
                time.sleep(5)
                driver.quit()
                driver = open_chrome_instance(exe_path, option, caps)
            bar()
        time.sleep(60)
        driver.quit()

    convert_scripts_json_to_csv(out_folder, scripts)
    logging.info('PROCESS COMPLETED SUCCESSFULLY')
    logging.info('TIME TAKEN: {} sec'.format(time.time() - start_time))
