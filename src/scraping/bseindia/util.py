import datetime
import json
import logging
import os

import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver

from utilities import UNIQUE_ID


def open_chrome_instance(exe_path, option, caps):
    driver = webdriver.Chrome(executable_path=exe_path, desired_capabilities=caps, options=option)
    return driver


def get_all_scripts_bse_india(driver):
    """
    It will get all active scripts for Equity
    """

    # EQUITY
    logging.info('EXTRACTING SCRIPTS FOR BSE-EQUITY')
    link = 'https://api.bseindia.com/BseIndiaAPI/api/ListofScripData/w?Group=&Scripcode=&industry=&segment=Equity&status=Active'
    driver.get(link)
    html = driver.page_source
    text = BeautifulSoup(html, 'html.parser').get_text()
    scripts = json.loads(text)
    logging.info('SUCCESS...')
    return scripts


def download_script_data_bse_india(driver, script_id):
    date = datetime.datetime.now()
    if len(str(date.day)) == 1:
        date_day = "0" + str(date.day)
    else:
        date_day = str(date.day)
    link = 'https://api.bseindia.com/BseIndiaAPI/api/StockPriceCSVDownload/w?pageType=0&rbType=D&Scode={}&FDates=01/01/1950&TDates={}'.format(
        script_id, "{}/{}/{}".format(date_day, date.month, date.year))
    driver.get(link)


def convert_scripts_json_to_csv(out_folder, scripts):
    csv_path = os.path.join(out_folder, 'scripts_{}.csv'.format(UNIQUE_ID))
    csv_file = open(csv_path, "w+")
    for i, script in enumerate(scripts):
        if i == 0:
            csv_file.write("{}\n".format(",".join(script.keys())))
        csv_file.write("{}\n".format(",".join([str(i) for i in script.values()])))
    csv_file.close()


def get_script_portfolio(script, out_folder):
    nse_df = pd.read_csv("https://archives.nseindia.com/content/equities/EQUITY_L.csv")
    script_id = script['SCRIP_CD']
    script_tag = script['scrip_id']
    isin_number = script['ISIN_NUMBER']
    try:
        script_url = "https://www.screener.in/company/{}/consolidated/".format(script_tag)
        tables = pd.read_html(script_url)
        dataframe = pd.DataFrame()
        for df in tables:
            if 'Unnamed: 0' in df.columns:
                dfx = df.set_index(['Unnamed: 0']).T
                dataframe = pd.concat([dataframe, dfx], axis=1)
            dataframe = dataframe.fillna('')
        dataframe.columns = dataframe.columns.str.encode('ascii', 'ignore').str.decode('ascii')
        csv_path = os.path.join(out_folder, 'data', 'portfolio_{}.csv'.format(script_id))
        dataframe.to_csv(path_or_buf=csv_path, encoding='utf-8')
        logging.info("COMPANY PORTFOLIO CREATED: BSE")
    except:
        logging.info('SCRIPT NOT FOUND ON BSE TRYING FOR NSE SYMBOL')
        try:
            script_tag = nse_df[nse_df[' ISIN NUMBER'] == 'INE208C01025']['SYMBOL'].tolist()[0]
            script_url = "https://www.screener.in/company/{}/consolidated/".format(script_tag)
            tables = pd.read_html(script_url)
            dataframe = pd.DataFrame()
            for df in tables:
                if 'Unnamed: 0' in df.columns:
                    dfx = df.set_index(['Unnamed: 0']).T
                    dataframe = pd.concat([dataframe, dfx], axis=1)
                dataframe = dataframe.fillna('')
            dataframe.columns = dataframe.columns.str.encode('ascii', 'ignore').str.decode('ascii')
            csv_path = os.path.join(out_folder, 'data', 'portfolio_{}.csv'.format(script_id))
            dataframe.to_csv(path_or_buf=csv_path, encoding='utf-8')
            logging.info("COMPANY PORTFOLIO CREATED: NSE")
        except Exception as e:
            logging.error("Error for Script: {}: {}".format(script['Scrip_Name'], e))
